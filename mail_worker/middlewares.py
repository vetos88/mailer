"""
    Общие(промежуточне) обработчики
"""
from aiohttp import web
from aiohttp.web_response import json_response


def error_pages(overrides):
    """
        Миддваре переобпределения ответов с определенными статусами.
    """
    @web.middleware
    async def middleware(request, handler):
        """
            Установка специфичных ответов.
        """
        try:
            response = await handler(request)
            override = overrides.get(response.status)
            if override is None:
                return response
            else:
                return await override(request, response)
        except web.HTTPException as ex:
            override = overrides.get(ex.status)
            if override is None:
                raise
            else:
                return await override(request, ex)

    return middleware


async def handle_404(request, response):
    """
        json ответ для кода 404
    """
    response = json_response({
        'error': 'end-point not found'
    }, status=404)
    return response


async def handle_500(request, response):
    """
        json ответ для кода 500
    """
    response = json_response({
        'error': 'internal server error'
    }, status=500)
    return response


def setup_middlewares(app):
    """
        Подключение промежутоных слоев
    """
    error_middleware = error_pages({404: handle_404,
                                    500: handle_500})
    app.middlewares.append(error_middleware)
