"""
    Конфигурация роутов приложения
"""
import time

from handlers import start_mailing, json_response


async def test_service(request):
    """
        Обработчик запроса на старт рассылки
    """
    requested_host = request.host
    headers = dict(request.headers.items())
    ip_address = request.headers.get('X-Forwarded-For', None)
    if ip_address is None:
        ip_address = request.transport.get_extra_info('peername', (None, None))[0]
    return json_response({'status': {
        'time': time.asctime(),
        'requested_addr': ip_address,
        'headers': headers,
        'requested_host': requested_host
    }})


def setup_routes(app):
    """
     Настройка роутера
    """

    app.router.add_get(r'/test', test_service)
    app.router.add_post(r'/distribution', start_mailing)
