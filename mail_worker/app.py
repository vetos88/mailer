"""
    Корневой модуль. Инициализация инстанса приложения.
"""
import logging
from logging.config import dictConfig

import uvloop
import asyncio
from aiohttp import web

from config import APP_PORT, APP_HOST, logging_config, MAIL_WORKER_APP_NAME, MAIL_WORKER_DEBUG
from config.logging_config import ACCESS_LOG_FORMAT
from middlewares import setup_middlewares
from router import setup_routes

dictConfig(logging_config.LOGGING_CONFIG)
logger = logging.getLogger(MAIL_WORKER_APP_NAME)


def init_and_run():
    """
        Функция инициализации и запуска приложения.
    """
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

    # variables_usage = 'Используемые значения\nCервер OTP = {otp_server},\nSMG-вебхук = {smg_web_hook}' \
    #     .format(**SERVER_DO_TEST)
    # loop = asyncio.get_event_loop()
    logger.debug('DEBUG MODE!')
    app = web.Application(debug=MAIL_WORKER_DEBUG)
    setup_middlewares(app)
    setup_routes(app)

    web.run_app(app, host=APP_HOST, port=APP_PORT, access_log_format=ACCESS_LOG_FORMAT)
