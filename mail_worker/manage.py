"""
    Точка входа. Модуль управления приложением.
"""
import argparse
from app import init_and_run

VERSION = "1.0.dev7"

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Action on running')
    subparsers = parser.add_subparsers(dest="command", help='Mode command')
    subparsers.add_parser('print_version', help='Print application version')
    args = parser.parse_args()
    if args.command == 'print_version':
        print(VERSION)
        exit(0)
    init_and_run()
