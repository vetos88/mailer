"""
    Обработчики запросов
"""
import asyncio
import re
from json import JSONDecodeError

from aiohttp.web_response import json_response

from config.logging_config import produce_logger
from handlers.utils import process_dispathing

logger = produce_logger('handlers')


async def start_mailing(request):
    """
        Обработчик запроса на старт рассылки
    """
    try:
        request_payload = await request.json()
    except JSONDecodeError:
        request_payload = await request.text()
        request_payload = re.sub(r'\s+', ' ', request_payload)
        request_payload = request_payload[:50]
        logger.warning('Get bad payload: %s....', request_payload)
        # TODO сделать через выброс исключения
        return json_response({'error': 'Bad payload Data!'}, status=400)
    else:
        asyncio.ensure_future(process_dispathing(request_payload))

        response_payload = {
            'message': 'Response OK. Emailing start processing!'
        }
        return json_response(response_payload)
