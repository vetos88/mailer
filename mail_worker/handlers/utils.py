"""
    Модуль с дополнительным функционалом
"""
import asyncio
import itertools

import aiohttp

from config import SEND_TASK_CHUNK
from config.logging_config import produce_logger
# from errors import BadTaskParams
from mail import DistributionTask
from mail.mail_factory import EmailFactory
from mail.sender_worker import AsyncSmtpManager

logger = produce_logger('handlers.utils')


async def process_dispathing(request_payload):
    """
        Функция управляющая процессом отправки писем и уведомлеления клиента.
    """
    # Пробуем распарсить полученный ответ от клиента
    try:
        if not isinstance(request_payload, dict):
            raise KeyError
        distribution_data = request_payload['distribution_data']
        call_back_url = request_payload['call_back_url']
    except KeyError:
        logger.warning('Bad distribution_data was provided %s', str(request_payload))
        # raise BadTaskParams
        return

    # Формируем задачи на обработку
    distribution_tasks = DistributionTask.distribution_tasks_factory(distribution_data)

    tasks_count = len(distribution_tasks)
    if len(distribution_data) != tasks_count:
        logger.warning(
            'Not all client data was parsed. distribution_data len %s, created_tasks count %, call_back %s',
            len(distribution_data), tasks_count, call_back_url
        )
    logger.info('Dispatch processing was started. call_back_url %s, distribution task count %s',
                call_back_url, tasks_count)

    # Формируем обьеты писем
    distribution_tasks = EmailFactory().create_emails(distribution_tasks)

    # Начинаем процесс отправки.
    async with AsyncSmtpManager() as manager:
        for shift in range(0, len(distribution_tasks), SEND_TASK_CHUNK):
            task_chunk = distribution_tasks[shift:shift + SEND_TASK_CHUNK]
            processed_tasks = await manager.send_emails(task_chunk)
            statuses_response = dict(map(lambda d_task: d_task.status_as_tuple(), processed_tasks))
            if call_back_url:
                try:
                    async with aiohttp.ClientSession() as session:
                        async with session.post(call_back_url, json=statuses_response, timeout=10) as resp:
                            resp_text = await resp.text()
                            logger.info(
                                'Notify to %s, status %s, response %s', call_back_url, resp.status, resp_text
                            )
                except (aiohttp.ClientError, asyncio.TimeoutError):
                    logger.info('Failed notify to %s', call_back_url, exc_info=True)
            else:
                logger.info('Notify not possible call_back_url not provided.')
    logger.info('Dispatch processing completed. call_back_url %s', call_back_url)
    return
