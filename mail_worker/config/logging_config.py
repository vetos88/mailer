"""
Настройки логгирования
"""

# Расширенный формат сообщей.
import logging

from raven.handlers.logging import SentryHandler

from . import MAIL_WORKER_APP_NAME, MAIL_WORKER_DEBUG, MAIL_WORKER_SENTRY_DSN, LOGGER_LEVEL

from raven import Client
from raven_aiohttp import AioHttpTransport

root_format = """%(levelname)s|%(asctime)s|%(filename)s|%(module)s|%(lineno)d; message:%(message)s"""

AVAILABLE_LOGGER_LEVELS = [
    'DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'
]


class AsyncSentryHandler(SentryHandler):
    """
        Асинхронный класс оюработчика для raven клиента.
    """

    def __init__(self, *args, **kwargs):
        kwargs['client'] = Client(transport=AioHttpTransport, *args, **kwargs)
        super().__init__(*args, **kwargs)


def produce_logger(name: str):
    """
        Получение дочернего логгера.
    """
    logger = logging.getLogger(f'{MAIL_WORKER_APP_NAME}.{name}')
    return logger


def set_logger_level():
    """
        Принудительное задание уровня логгирования.
        В случае если принудительно уровень логгирования не задан
        то значение назанчаетсяв соответсвии с переменной  DEBUG_MODE
    """
    if LOGGER_LEVEL in AVAILABLE_LOGGER_LEVELS:
        return LOGGER_LEVEL
    return None


MANUAL_LOGGER_LEVEL = set_logger_level()

# формат логгирования для логгера доступа aiohttp
ACCESS_LOG_FORMAT = '%a "%r" %s %b "%{Referer}i" %{User-Agent}i (process_time: %Tf)'

# Полная конфигурация логгинга
LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '|%(name)s| %(levelname)s - [%(asctime)s] - %(filename)s - %(module)s|%(lineno)d: "%(message)s"',
            'datefmt': "%H:%M:%S %d.%m.%Y",
        },
        'info': {
            'format': '%(levelname)s:[%(asctime)s]:"%(message)s"',
            'datefmt': "%H:%M:%S %d.%m.%Y",
        },
        'access': {
            'format': ' |%(name)s| %(levelname)s - [%(asctime)s] - %(message)s',
            'datefmt': "%H:%M:%S %d.%m.%Y",
        }
    },
    'handlers': {
        'debug_console': {
            'level': 'DEBUG',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
        'access_console': {
            'level': 'DEBUG',
            'formatter': 'access',
            'class': 'logging.StreamHandler',
        },
        'info_console': {
            'level': 'INFO',
            'formatter': 'info',
            'class': 'logging.StreamHandler',
        },
        'warning_console': {
            'level': 'WARNING',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
        'err_console': {
            'level': 'ERROR',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
        'sentry_warn': {
            'level': 'WARNING',
            'class': 'config.logging_config.AsyncSentryHandler',
            'dsn': MAIL_WORKER_SENTRY_DSN,
            'auto_log_stacks': True,
            'ignore_exceptions': ['KeyboardInterrupt']
        },
        'sentry_error': {
            'level': 'ERROR',
            'class': 'config.logging_config.AsyncSentryHandler',
            'dsn': MAIL_WORKER_SENTRY_DSN,
            'auto_log_stacks': True,
            'ignore_exceptions': ['KeyboardInterrupt']
        }
    },
    'loggers': {
        '': {
            'handlers': ['debug_console'] if MAIL_WORKER_DEBUG else ['warning_console', 'sentry_error'],
            'level': 'DEBUG' if MAIL_WORKER_DEBUG else 'WARNING',
        },
        'aiohttp.access': {
            'handlers': ['access_console'] if MAIL_WORKER_DEBUG else ['access_console', 'sentry_error'],
            'level': 'DEBUG' if MAIL_WORKER_DEBUG else 'WARNING',
            'propagate': False
        },
        MAIL_WORKER_APP_NAME: {
            'handlers': ['debug_console', 'sentry_warn'] if MAIL_WORKER_DEBUG else ['info_console', 'sentry_warn'],
            'level': 'DEBUG' if MAIL_WORKER_DEBUG else 'INFO',
            'propagate': False
        },
        'sentry.errors': {
            'handlers': ['err_console'],
            'level': 'ERROR',
            'propagate': False
        },
        'sentry.errors.uncaught': {
            'handlers': ['err_console'],
            'level': 'ERROR',
            'propagate': False
        }
    }
}
