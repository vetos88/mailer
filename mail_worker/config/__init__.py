"""
    Модуль в котором происходи считывание настроек из среды окружения
"""

import os
import socket
from dotenv import load_dotenv
load_dotenv(dotenv_path='../env_config/service_config')


MAIL_WORKER_APP_NAME = os.environ.get('MAIL_WORKER_APP_NAME') or 'MAILER'

# Режим дебага
MAIL_WORKER_DEBUG = os.environ.get('MAIL_WORKER_DEBUG', 'TRUE').upper() == 'TRUE'
LOGGER_LEVEL = os.environ.get('LOGGER_LEVEL', '')

# Адрес по котрому доступен сервис
APP_HOST = os.environ.get('APP_HOST') or "0.0.0.0"

# Порт по котрому доступен сервис
APP_PORT = int(os.environ.get('APP_PORT') or 8882)

# настройки для сентри
MAIL_WORKER_SENTRY_DSN = os.environ.get('MAIL_WORKER_SENTRY_DSN') or ''

# Размер буфера задач после которого происходит уведомление о статусе отправки
SEND_TASK_CHUNK = int(os.environ.get('SEND_TASK_CHUNK') or 5)

# Настройки электронного письма
DNS_NAME = os.environ.get('DNS_NAME') or socket.getfqdn()

EMAIL_DEFAULT_FROM_EMAIL = os.environ.get('EMAIL_DEFAULT_FROM_EMAIL') or 'LRM@zoloto585.ru'
EMAIL_DEFAULT_EMAIL_SUBJECT = os.environ.get('EMAIL_DEFAULT_EMAIL_SUBJECT') or 'Email Distribution'

EMAIL_USE_LOCALTIME = True

# Настройки почтового сервера
EMAIL_HOST = os.environ.get('EMAIL_HOST') or 'spb-s-relay.zoloto585.int'
EMAIL_PORT = int(os.environ.get('EMAIL_PORT') or 25)

EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')

EMAIL_USE_TLS = os.environ.get('EMAIL_USE_TLS', 'FALSE').upper() == 'TRUE'
EMAIL_USE_STARTTLS = os.environ.get('EMAIL_USE_STARTTLS', 'FALSE').upper() == 'TRUE'

"""
Количество попыток переподключения при неудачной попытке установить соединение с почтовым сервисом.
Время между попытками вычисляется по формуле 2*номер попытки*10 секунд.
"""
EMAIL_RECONNECT_RETRIES = int(os.environ.get('EMAIL_RECONNECT_RETRIES') or 5)

# Время ожидания ответа от сокета почтового сервера при попытке соединения.
EMAIL_TIMEOUT = int(os.environ.get('EMAIL_TIMEOUT') or 60)

# Кодировака приложения
DEFAULT_CHARSET = 'utf-8'
