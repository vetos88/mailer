"""
    Модуль формирования элеткроных писем.
"""
import re
from email.mime.text import MIMEText
from email.utils import formatdate, make_msgid
from typing import List

from config import EMAIL_DEFAULT_FROM_EMAIL, DEFAULT_CHARSET, EMAIL_USE_LOCALTIME, DNS_NAME, EMAIL_DEFAULT_EMAIL_SUBJECT
from config.logging_config import produce_logger
from mail import DistributionTask

logger = produce_logger('mail_factory')


class EmailFactory:
    """
    Класс создания электонных писем
    """

    def __init__(self):
        self.from_email = EMAIL_DEFAULT_FROM_EMAIL
        self.charset = DEFAULT_CHARSET

    def create_message(self, recipient: str, email_body: str, subject=EMAIL_DEFAULT_EMAIL_SUBJECT) -> MIMEText:
        """
            Создание обьекта message по предоставленным данным.
        """
        # Если вдруг осутсвует заголовок письма формирует его из первых 50 символов или на всякий случай обрезаем.
        logger.debug('Creating email of %s  type for recipient %s ', subject, recipient)
        msg = MIMEText(email_body, _subtype='html', _charset=self.charset)

        # Произведем небольшое форматирование заголовка.
        msg['Subject'] = re.sub('\s+', ' ', subject)[:78]
        msg['From'] = self.from_email
        msg['To'] = recipient
        msg['Date'] = formatdate(localtime=EMAIL_USE_LOCALTIME)
        msg['Message-ID'] = make_msgid(domain=DNS_NAME)
        logger.debug('Email for %s created. len: %s', recipient, len(msg.get_payload()))
        return msg

    def create_emails(self, distribution_tasks: List[DistributionTask]) -> List[DistributionTask]:
        """
            DistributionTask формиркует на основе его данных обьект письма
            и записывает его в DistributionTask.
            Возвращает массив модифицированных DistributionTask
        """

        if not isinstance(distribution_tasks, list) or not len(distribution_tasks):
            logger.info('Recipients not found, cant create mails')
            return distribution_tasks

        for task in distribution_tasks:
            task.msg = self.create_message(task.recipient_email, task.email_body, task.subject)
            task.status = DistributionTask.MESSAGE_FORMED
        return distribution_tasks
