"""
    модуль подготовки и отправки писем
"""
from contextlib import suppress
from typing import Union

from config.logging_config import produce_logger
from errors import BadTaskParams

logger = produce_logger('mail')


class DistributionTask:
    """
        Класс инкапсулирующий данные по об одной задачи на отпраку письма.
    """

    RAW_DATA = 'raw_data_only'
    MESSAGE_FORMED = 'message_object_formed'
    SENT_OK = 'OK'
    SENT_FAIL = 'FAIL'

    def __init__(self, task_id: Union[str, int], task_dict: dict, *args, **kwargs):
        """
            Идентификаторо задания и словарь с данными для выполения.
        """
        self._task_id = str(task_id)
        try:
            if not isinstance(task_dict, dict):
                raise KeyError
            self.recipient_email = str(task_dict['email'])
            self.email_body = str(task_dict['body'])
            self.subject = str(task_dict['subject'])
        except KeyError:
            logger.info('Bad task params was provided %s', str(task_dict))
            raise BadTaskParams
        # Сформированнон письмо обьект типа MIMEText
        self.msg = None
        self.status = self.RAW_DATA

    @property
    def task_id(self):
        """
            Геттер для свойства идентификатороа задания на расслыку.
        """
        return self._task_id

    @classmethod
    def distribution_tasks_factory(cls, payload_bunch: dict):
        """
            Создание массива оьектов DistributionTask
            ожидется словарь с данными задач на рассылку
            {
                 идентификатор_задачи: {
                        "email": "емайл адресата",
                        "body": "текст письма.",
                        "subject": "заголовок письма."
                    },
                 другой_идентификатор_задачи: {
                        "email": "емайл адресата",
                        "body": "текст письма.",
                        "subject": "заголовок письма."
                    }, ...
            },
            возвращает массив обьектов DistributionTask
        """
        tasks = []
        if isinstance(payload_bunch, dict):
            for ident, payload in payload_bunch.items():
                with suppress(BadTaskParams):
                    tasks.append(cls(ident, payload))
        return tasks

    def status_as_tuple(self):
        """
            Возвращает кортеж в виде
            self._task_id, self.status

        """
        return self._task_id, self.status
