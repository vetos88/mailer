"""
    Функции выполняющие рассылку писем.
"""
from typing import List

from config import EMAIL_HOST_USER, EMAIL_HOST_PASSWORD, EMAIL_USE_TLS, EMAIL_USE_STARTTLS, EMAIL_HOST, EMAIL_PORT, \
    EMAIL_TIMEOUT, DNS_NAME, EMAIL_RECONNECT_RETRIES
from config.logging_config import produce_logger
from collections import namedtuple

import aiosmtplib

import asyncio

from mail import DistributionTask

logger = produce_logger('sender_manager')


class AsyncSmtpManager:
    """
    Класс оберка для управления SMTP подключением.
    """

    def __init__(self, keep_connection=None):

        self.username = EMAIL_HOST_USER
        self.password = EMAIL_HOST_PASSWORD

        self.use_tls = EMAIL_USE_TLS
        self.use_starttls = EMAIL_USE_STARTTLS

        # # TODO возможно оповещения перенести в настройки
        # if not settings.EMAIL_USE_TLS and not settings.EMAIL_USE_STARTTLS:
        #     log.warning('No one TLS, or STARTTLS not enable! Not secure connection will used!')

        if EMAIL_USE_TLS and EMAIL_USE_STARTTLS:
            logger.warning('Enabled TLS, and STARTTLS option!,TLS will be used TLS')
            self.use_starttls = False

        self.client = aiosmtplib.SMTP(
            hostname=EMAIL_HOST,
            port=EMAIL_PORT,
            timeout=EMAIL_TIMEOUT,
            source_address=DNS_NAME,
            use_tls=EMAIL_USE_TLS,
            validate_certs=True,
            tls_context=None,  # ssl.SSLContext
            cert_bundle=None,  # str
            loop=None  # EventLoop
        )
        self.connect_retries = EMAIL_RECONNECT_RETRIES
        self.keep_connection = keep_connection
        self._is_open = False

    async def _init_connection(self):
        """
        Метод установки соединения с сервером.
        Пробует соеденитьсяс серверром заданное количесво раз (указано в параметре self.connect_retries)
        В случае не успеха возвращает None.
        """
        conn_status = aiosmtplib.SMTPStatus.closing
        for i in range(self.connect_retries):
            try:
                logger.debug('Try to connect %s:%s.', self.client.hostname, self.client.port)
                response = await self.client.connect(timeout=10)
                if response.code != aiosmtplib.SMTPStatus.ready:
                    logger.warning(
                        'Server connection is established. But status code is not READY(220), response: %s', response
                    )
                if self.use_starttls:
                    response = await self.client.starttls()
            except (
                    aiosmtplib.errors.SMTPConnectError,
                    aiosmtplib.errors.SMTPTimeoutError,
                    aiosmtplib.errors.SMTPServerDisconnected,
            ) as ex:
                msg = 'SMTP connection to %s:%s retry unsuccessful. exception: %s: %s'
                logger.error(
                    msg, self.client.hostname, self.client.port, type(ex), ex
                )
                self.client.close()
                conn_status = aiosmtplib.SMTPStatus.closing
                logger.debug(
                    '%s from %s retries left. ', self.connect_retries - (i + 1), self.connect_retries
                )
                is_reconnect = (i < self.connect_retries - 1)
                if is_reconnect:
                    time_to_reconnect = (i + 1) * 2
                    # time_to_reconnect = (i + i) * 10
                    logger.debug('Wait %s seconds before reconnect. ', time_to_reconnect)
                    await asyncio.sleep(time_to_reconnect)
                    logger.debug('Reconnecting...')
                else:
                    logger.error('Connection to %s:%s failed!', self.client.hostname, self.client.port)
            except aiosmtplib.errors.SMTPException as ex:
                msg = 'SMTP connection to %s:%s failed!. exception: %s: %s'
                logger.error(
                    msg, self.client.hostname, self.client.port, type(ex), ex
                )
                conn_status = aiosmtplib.SMTPStatus.closing
                self.client.close()
                break
            else:
                conn_status = aiosmtplib.SMTPStatus.ready
                msg = 'Connection with server is established. Response: %s, connection status set %s'
                logger.debug(msg, response, conn_status)
                break
        return conn_status

    async def _server_authorization(self):
        """
        Метод получения авторизированного доступа на почтовый сервер.
        Если предоставляются авторизационные данные метод предпринимает одну попытку аторизоваться.
        В случае ответа auth_successful = 235, метод возвращает ответ завершенной операции.
        Если авторизационные данные не предоставленны, то метод возвращает статус завершенной операции.
        Любые другие ответы следует рассматривать как ошибку.
        В лучае предоставления учетных данных и не возможности получить ответ со статусом 235,
        метод закрывает соединение.
        """
        auth_status = aiosmtplib.SMTPStatus.completed
        if self.username and self.password:
            logger.debug(
                'SMTP credential is provided. username: %s, password: %s. Try to auth...', self.username, self.password
            )
            try:
                response = await self.client.login(username=self.username, password=self.password)
            except aiosmtplib.errors.SMTPAuthenticationError as ex:
                logger.error('%s', ex.message)
                auth_status = aiosmtplib.SMTPStatus.auth_failed
                self.client.close()
            except aiosmtplib.errors.SMTPException as ex:
                auth_status = aiosmtplib.SMTPStatus.auth_failed
                logger.error('%s', ex.message)
                self.client.close()
            else:
                if response.code != aiosmtplib.SMTPStatus.auth_successful:
                    auth_status = aiosmtplib.SMTPStatus.auth_failed
                    logger.error('Auth failed, server response %s, auth status set', response, auth_status)
                    self.client.close()
                else:
                    auth_status = aiosmtplib.SMTPStatus.completed
                    logger.debug('Response auth from server %s auth status set %s', response, auth_status)
        else:
            logger.debug('SMTP credential is not provided.')
            auth_status = aiosmtplib.SMTPStatus.completed
        return auth_status

    async def open(self):
        """
        Метод для регулярного открытия соединения перед отправкой письма.
        В случает плохих ответов от сервера метод возвращает None
        """
        init_connection_status = await self._init_connection()
        if init_connection_status != aiosmtplib.SMTPStatus.ready:
            return None
        authorization_status = await self._server_authorization()
        if authorization_status is None or authorization_status != aiosmtplib.SMTPStatus.completed:
            return None
        self._is_open = True
        return init_connection_status

    async def check_mail_server(self):
        """
        Метод для стартовой проверки состояния и параметров SMTP сервера.
        Проверяется доступность и возможность совершить авторизацию.
        В случае плохих отвертов от сервера метод возвращает None.
        """
        init_connection_status = await self._init_connection()
        if init_connection_status != aiosmtplib.SMTPStatus.ready:
            return None
        server_status_response = await self.client.ehlo()
        logger.info('Server status response %s', server_status_response)
        if self.username and self.password:
            logger.info(
                'SMTP credential is provided. username: %s, password: %s. Try to auth...', self.username, self.password
            )
            status = await self._server_authorization()
            if status != aiosmtplib.SMTPStatus.completed:
                logger.error('Credential provided. authorisation is required. authorisation is failed.')
                return None
        await self.client.quit()
        return server_status_response

    async def _send_message(self, message):
        """
        Метод выполняет отправку одного объекта message.
        Отправка нескольких обьектов через одно соединение выполняется синхронно
        """
        SndRes = namedtuple('SndRes', 'recipient is_sent')
        result = None
        rec = message.get_all('To', 'No Recipients')
        try:
            logger.debug('Sending message to %s', rec)
            response = await self.client.send_message(message)
        except ValueError as ex:
            err_msg = ex.args
            logger.error(
                'Bad message. Message Not Sent to recipient %s. exception %s', rec, err_msg
            )
            result = SndRes(rec, False)
        except (aiosmtplib.SMTPResponseException, aiosmtplib.SMTPRecipientsRefused):
            rec = message.get_all('To', 'No Recipients')
            logger.exception('In sending message to %s. Try to sending messages continue', rec)
            result = SndRes(rec, False)
        except aiosmtplib.SMTPException:
            logger.exception('Dispatching emails was crashed')
            result = SndRes(rec, False)
        except Exception:
            logger.critical('Dispatching emails was crashed', exc_info=True)
            result = SndRes(rec, False)
        else:
            logger.debug('Message send on server to recipient %s, sending result %s', rec, response)
            #  TODO изучить возможность неуспешных ответов сервера.
            if 'response is bad':
                pass  # Заглушка
            result = SndRes(rec, True)
        return result

    async def send_emails(self, distribution_tasks: List[DistributionTask],
                          keep_connection=False) -> List[DistributionTask]:
        """
        Метод выполняет отправку сформированных писем.
        """
        if not distribution_tasks:
            logger.warning('Messages for sending not provided')
            return distribution_tasks
        if not self._is_open:
            await self.open()
            if not self._is_open:
                logger.debug('Connection not opened. Messages not sent')
                for task in distribution_tasks:
                    task.status = DistributionTask.SENT_FAIL
                return distribution_tasks
        count_messages = len(distribution_tasks)
        logger.debug('Connection opened %s messages provided to dispatching', count_messages)
        # tasks = [asyncio.ensure_future(self._send_message(task)) for task in distribution_tasks]
        # to_do_iter = asyncio.as_completed(tasks)

        for task in distribution_tasks:
            result = await self._send_message(task.msg)
            if result and result.is_sent:
                task.status = DistributionTask.SENT_OK
            else:
                task.status = DistributionTask.SENT_FAIL

        if self.keep_connection or keep_connection:
            return distribution_tasks
        await self.close()
        return distribution_tasks

    async def close(self):
        """
            Метод для разрыва связи с серврром отправки.
        """
        try:
            result = await self.client.quit()
        except (aiosmtplib.SMTPResponseException, aiosmtplib.SMTPException):
            logger.error('Closing connection is crashed')
        else:
            logger.debug('Connection to server closed, result %s', result)
        finally:
            self._is_open = False

    async def __aenter__(self):
        self.keep_connection = True
        await self.open()
        return self

    async def __aexit__(self, exc_type, exc, tb):
        await self.close()
