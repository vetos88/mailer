FROM python:3.6.5-slim-stretch

# Обновляем индексы и пакеты.
RUN apt-get update && apt-get upgrade -y

ENV PYTHONUNBUFFERED 1

# Подготаваем окружение.
COPY ./requirements /requirements
RUN pip install -r requirements/prod.txt

RUN mkdir /mail_worker
ADD mail_worker/ /mail_worker

WORKDIR /mail_worker
ENTRYPOINT ["python", "manage.py"]