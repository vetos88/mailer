#!/usr/bin/env bash
# Менеджер управления процесами развертывания и эксплуатации.
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m'
# Список доступных команд. Команда передается первым аргументом.
# Запуск без аргументов выполняет сборку образа и старт контейнера приложения.
commands=(
"help"
"build"
)

ldt_registry="registry.zoloto585.int:10443"
image_tag="ldt/mailer-service"
src_folder="mail_worker"

if [[ " ${commands[@]} " =~ " $1 " ]]; then
    if [[ "$1" == ${commands[0]} ]]; then
    # Список всех команд.
        echo -e "${GREEN}Аvaliable commands:${NC}"
        echo ${commands[*]}
    else
        echo -e "${GREEN}$1 - command was provided.${NC}"
        if [[ "$1" == ${commands[1]} ]]; then
            if [ -f .env  ]; then
                export $(cat .env)
            else
                echo -e "${YELLOW}.env file not found,${NC} custom settings not provided. Use default params.
            ${YELLOW}Registry may not be available!${NC}"
            fi
            registry=${CUSTOM_REGISTRY:-$ldt_registry}
            # Сборка образа приложения и помещение его в хранилише
            echo -e "${GREEN}New image will build.${NC}"
            docker build -t ${image_tag}:latest --no-cache .
            curr_version=$(docker run ${image_tag}:latest print_version)
            echo -e "Please set version (current: ${GREEN}${curr_version}${NC})"
            read user_version
            curr_version=${user_version:-$curr_version}
            echo -e "Image with tag ${GREEN} ${image_tag}:${curr_version} ${NC} will create!"
            docker tag ${image_tag}:latest ${registry}/${image_tag}:${curr_version}
            for (( ; ; ))
            do
                echo -e "Do you want push image to repo(image will remove locally)? registry: ${YELLOW} $registry ${NC}(Y/n)"
                read user_decision
                if [[ "$user_decision" == "Y" || "$user_decision" == "n" ]]; then
                    break
                else
                    echo -e "${RED}Y or n only!${NC}"
                fi
            done
            if [[ "$user_decision" == "Y" ]]; then
                docker login -u ${REGISTRY_LOGIN} -p ${REGISTRY_PASSWORD} ${registry}
                docker push ${registry}/${image_tag}:${curr_version}
                echo -e "Image ${YELLOW}${image_tag}:${curr_version} ${NC} in repo!"
                docker rmi ${image_tag}:${curr_version}
                docker rmi ${registry}/${image_tag}:${curr_version}
            fi
            echo -e "${GREEN}Build complete! ${NC}"
        fi
    fi
else
    echo -e "${RED}$0 $1 - Bad command provided!${NC} ${YELLOW}Provide one of: [${commands[*]}]. ${NC}"
fi